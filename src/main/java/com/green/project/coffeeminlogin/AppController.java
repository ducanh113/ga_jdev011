package com.green.project.coffeeminlogin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.green.project.coffeeminlogin.models.Product;
import com.green.project.coffeeminlogin.services.ProductService;

@Controller
public class AppController {
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping("/")
	public String viewHomePage(Model model) {
		List<Product> listProduct = productService.getAllProduct();
		
		model.addAttribute("listProduct", listProduct);
		
		return "index";
	}
	@RequestMapping("/new")
	public String showNewProduct(Model model) {
		Product product = new Product();
		model.addAttribute("product", product);
		return "NewProducts";
	}
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("product") Product product) {
		productService.save(product);
		return "redirect:/";
	}
	@RequestMapping("edit/{id}")
	public ModelAndView showEditProduct(@PathVariable(name = "id")Long id) {
		ModelAndView modelAndView = new ModelAndView("edit_product");
		Product product = productService.get(id);
		modelAndView.addObject("product", product);
		return modelAndView;
	}
	@RequestMapping("delete/{id}")
	public String deleteProduct(@PathVariable(name = "id")Long id) {
		productService.delete(id);
		return "redirect:/";
	}
}
