package com.green.project.coffeeminlogin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.green.project.coffeeminlogin.dao.ProductRepository;
import com.green.project.coffeeminlogin.models.Product;

@Service
public class ProductService {

	@Autowired
	private ProductRepository repo;
	
	public List<Product> getAllProduct() {
		return repo.findAll();
	}
	public void save(Product product) {
		repo.save(product);
	}
	public Product get(Long id) {
		return repo.findById(id).get();
	}
	public void delete(Long id) {
		repo.deleteById(id);
	}
}
